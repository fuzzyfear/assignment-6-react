import {
  ACTION_TRANSLATE_ATTEMPT,
  ACTION_TRANSLATE_SUCCESS,
  ACTION_TRANSLATE_SUCCESS_GET,
} from "../actions/translateActions";

export const translateMiddleware =
  ({ dispatch }) =>
  (next) =>
  (action) => {
    next(action);

    if (action.type === ACTION_TRANSLATE_ATTEMPT) {
    }
    if (action.type === ACTION_TRANSLATE_SUCCESS) {
    }
    if (action.type === ACTION_TRANSLATE_SUCCESS_GET) {
    }
  };
