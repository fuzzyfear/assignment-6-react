import { applyMiddleware } from "redux";
import { translateMiddleware } from "./translateMiddleware";

export default applyMiddleware(translateMiddleware);
