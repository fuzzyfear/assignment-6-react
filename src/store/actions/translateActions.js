export const ACTION_TRANSLATE_ATTEMPT = "[translate] ATTEMPT";
export const ACTION_TRANSLATE_SUCCESS = "[translate] SUCCESS";
export const ACTION_TRANSLATE_SUCCESS_GET = "[translate] SUCCESS_GET";
export const ACTION_TRANSLATE_ERROR = "[translate] ERROR";

export const translateAttemptAction = (inputText) => ({
  type: ACTION_TRANSLATE_ATTEMPT,
  payload: inputText,
});

export const translateSuccessAction = (inputText) => ({
  type: ACTION_TRANSLATE_SUCCESS,
  payload: inputText,
});

export const translateSuccessGetAction = () => ({
  type: ACTION_TRANSLATE_SUCCESS_GET,
});

export const translateFailedAction = (error) => ({
  type: ACTION_TRANSLATE_ERROR,
  payload: error,
});
