import {
  ACTION_TRANSLATE_ATTEMPT,
  ACTION_TRANSLATE_SUCCESS,
  ACTION_TRANSLATE_SUCCESS_GET,
  ACTION_TRANSLATE_ERROR,
} from "../actions/translateActions";

const initialState = {
  translateAttempt: false,
  translateError: "",
  translateText: "",
};

export const translateReducer = (state = initialState, action) => {
  switch (action.type) {
    case ACTION_TRANSLATE_ATTEMPT:
      return {
        ...state,
        translateAttempt: true,
        translateError: "",
      };
    case ACTION_TRANSLATE_SUCCESS:
      return {
        ...state,
        translateText: action.payload,
      };
    case ACTION_TRANSLATE_SUCCESS_GET:
      return {
        ...state,
        translateAttempt: true,
      };
    case ACTION_TRANSLATE_ERROR:
      return {
        ...state,
        translateAttempt: false,
        translateError: action.payload,
      };
    default:
      return state;
  }
};
