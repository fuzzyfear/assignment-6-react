import { combineReducers } from "redux";
import { translateReducer } from "./translateReducer";

const appReducer = combineReducers({
  translate: translateReducer,
});

export default appReducer;
