import { useHistory } from "react-router";

//Logs out the user and returns the user to the login page
const ProfileLogOut = () => {
  const history = useHistory();
  const logOut = () => {
    localStorage.clear();
    history.push("/");
    window.location.reload(false);
  };
  return (
    <div>
      <button onClick={logOut}>Log Out</button>
    </div>
  );
};

export default ProfileLogOut;
