//Displays the user's username
const ProfileUser = () => {
  let username = localStorage.getItem("username");
  return (
    <div>
      <h1>Profile Page</h1>
      <h3>{username}</h3>
    </div>
  );
};

export default ProfileUser;
