import { TranslationApi } from "../api";

//Deletes all translations for the given user
const ProfileDeleteTranslations = () => {
  const deleteTranslations = () => {
    let username = localStorage.getItem("username");
    TranslationApi.putClearTranslations(username);
  };
  return (
    <div>
      <button onClick={deleteTranslations}>Clear translations</button>
    </div>
  );
};

export default ProfileDeleteTranslations;
