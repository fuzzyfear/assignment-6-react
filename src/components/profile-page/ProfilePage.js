import ProfileUser from "./ProfileUser";
import ProfileLogOut from "./ProfileLogOut";
import ProfileTranslations from "./ProfileTranslations";
import ProfileDeleteTranslations from "./ProfileDeleteTranslations";

//Parent component for the profile page
const ProfilePage = () => {
  return (
    <div>
      <ProfileUser />
      <ProfileLogOut />
      <ProfileTranslations />
      <ProfileDeleteTranslations />
    </div>
  );
};
export default ProfilePage;
