import { useEffect, useState } from "react";
import { getUser as getUserFromApi } from "../api";

//Displays the users 10 latest translations
const ProfileTranslations = () => {
  let username = localStorage.getItem("username");
  const [translation, setTranslation] = useState([]);
  useEffect(() => {
    getUser(username);
  }, []);

  //Gets the given user's 10 latest translations
  const getUser = async (username) => {
    const user = await getUserFromApi(username);
    const allTranslations = user[0].translations.sort((a, b) => a - b);
    let limitedTranslations = [];
    let maxLength = allTranslations.length < 10 ? allTranslations.length : 10;
    for (let i = 0; i < maxLength; i++) {
      if (allTranslations[i].deleted === false) {
        limitedTranslations.push(allTranslations[i].translation);
      }
    }
    setTranslation(limitedTranslations);
  };
  return (
    <div>
      <h2>Latest Translations</h2>
      {translation.map((input) => {
        const charArray = [...input];
        console.log(charArray);
        return (
          <div className="translations" key={input}>
            <p>{input}</p>
            {charArray.map((i, index) => (
              <img key={index} src={`./signPictures/${i}.png`}></img>
            ))}
          </div>
        );
      })}
    </div>
  );
};

export default ProfileTranslations;
