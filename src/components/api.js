const URL = "http://localhost:8000/users";
const URL_USER = "http://localhost:8000/users?username=";

export const TranslationApi = {
  //Marks every translation for the given user as deleted
  async putClearTranslations(username) {
    const currentUser = await getUser(username);
    const translations = currentUser[0].translations;
    for (const translation of translations) {
      translation.deleted = true;
    }
    const updateUser = {
      username: username,
      translations: translations,
    };
    return fetch(URL + "/" + currentUser[0].id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updateUser),
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "unknown error occured" } = await response.json();
        throw new Error(error);
      }
      return response.json();
    });
  },

  //Adds a new translation to the user's translation history
  async putNewTranslation(username, translation) {
    const currentUser = await getUser(username);
    console.log(currentUser);
    const translations = currentUser[0].translations;
    console.log(translations);
    const translationWithBool = { translation: translation, deleted: false };
    console.log(translationWithBool);
    translations.push(translationWithBool);
    const updateUser = {
      username: username,
      translations: translations,
    };

    return fetch(URL + "/" + currentUser[0].id, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updateUser),
    }).then(async (response) => {
      if (!response.ok) {
        const { error = "unknown error occured" } = await response.json();
        throw new Error(error);
      }
      return response.json();
    });
  },

  //Adds a new user to the db
  async postNewUser(username) {
    let checkForOldUser = await getUser(username);
    if (checkForOldUser.length === 0) {
      let user = {
        username: username,
        translations: [],
      };

      return fetch(URL, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(user),
      });
    }
  },
};

//Gets the current user
export async function getUser(username) {
  let response = await fetch(URL_USER + username);
  let data = await response.json();
  return data;
}
