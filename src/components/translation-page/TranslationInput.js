import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { translateSuccessAction } from "../../store/actions/translateActions";

//Takes the users input and store it in a state
const TranslationInput = () => {
  const dispatch = useDispatch();

  const [inputText, setInputText] = useState({
    inputText: "",
  });
  useEffect(() => {
    dispatch(translateSuccessAction(inputText.inputText));
  }, []);

  const onInputChange = (event) => {
    setInputText({
      ...inputText,
      [event.target.id]: event.target.value,
    });
  };

  //Compares the input to see if it meets the requirements
  const onFormSubmit = (event) => {
    event.preventDefault();
    var letters = /^[A-Za-z]+$/;
    if (inputText.inputText.match(letters)) {
      dispatch(translateSuccessAction(inputText.inputText));
    } else {
      alert(
        "You can only translate alphabetical letters from english alphabet"
      );
    }
  };
  return (
    <form className="mb-3" onSubmit={onFormSubmit}>
      <input
        id="inputText"
        type="text"
        placeholder="Translate this text"
        className="form-control"
        onChange={onInputChange}
      ></input>
      <button type="submit" className="btn btn-primary btn-lg">
        🧙
      </button>
    </form>
  );
};

export default TranslationInput;
