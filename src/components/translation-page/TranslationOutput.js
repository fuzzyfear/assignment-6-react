import { useEffect } from "react";
import { useSelector } from "react-redux";
import { TranslationApi } from "../api";

//Displays the translation
const TranslationOutput = () => {
  const inputText = useSelector((state) => state.translate.translateText);
  useEffect(() => {
    if (inputText) {
      TranslationApi.putNewTranslation(
        localStorage.getItem("username"),
        inputText
      );
    }
  }, [inputText]);

  const charArray = Array.from(inputText);
  const charToPicture = charArray.map((i, index) => (
    <img key={index} src={`./signPictures/${i}.png`}></img>
  ));

  return (
    <div>
      <div className="output">{charToPicture}</div>
    </div>
  );
};

export default TranslationOutput;
