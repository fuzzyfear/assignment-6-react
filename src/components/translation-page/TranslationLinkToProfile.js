import { useHistory } from "react-router-dom";

//Routes the user to the profile
const TranslationLinkToProfile = () => {
  const history = useHistory();
  const linkToProfile = () => {
    history.push("/profile");
  };
  return (
    <div className="profile-button">
      <span className="material-icons" onClick={linkToProfile}>
        account_circle
      </span>
    </div>
  );
};
export default TranslationLinkToProfile;
