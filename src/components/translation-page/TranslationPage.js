import TranslationInput from "./TranslationInput";
import TranslationOutput from "./TranslationOutput";
import TranslationLinkToProfile from "./TranslationLinkToProfile";

//THe parent of the translation page
const TranslationPage = () => {
  return (
    <div>
      <TranslationLinkToProfile />
      <TranslationInput />
      <TranslationOutput />
    </div>
  );
};

export default TranslationPage;
