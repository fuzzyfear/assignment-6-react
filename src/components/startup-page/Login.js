import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { TranslationApi } from "../api";

//Stores the username in localStorage and sets up the user in the db.json file
const Login = () => {
  const [credentials, setCredentials] = useState({
    username: "",
  });

  const onInputChange = (event) => {
    setCredentials({
      ...credentials,
      [event.target.id]: event.target.value,
    });
  };
  const history = useHistory();
  const continueAfterLogin = () => {
    localStorage.setItem("username", credentials.username);
    TranslationApi.postNewUser(credentials.username);
    history.push("/translation");
  };

  return (
    <>
      {localStorage.getItem("username") && history.push("/translation")}
      {!localStorage.getItem("username") && (
        <main>
          <div>
            <input
              id="username"
              type="text"
              placeholder="Enter your username"
              onChange={onInputChange}
            ></input>
          </div>

          <button type="submit" onClick={continueAfterLogin}>
            Login
          </button>
        </main>
      )}
    </>
  );
};

export default Login;
