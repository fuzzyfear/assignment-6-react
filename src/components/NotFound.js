import { Link } from "react-router-dom";

//A page for when the user enters an invalid url
const NotFound = () => {
  return (
    <main>
      <h1>Page doesn't exists</h1>
      <Link to="/">Return home screen</Link>
    </main>
  );
};

export default NotFound;
