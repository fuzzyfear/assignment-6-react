import "./App.css";
import Login from "./components/startup-page/Login";
import NotFound from "./components/NotFound";
import TranslationPage from "./components/translation-page/TranslationPage";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import ProfilePage from "./components/profile-page/ProfilePage";

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <h1>Lost in translation</h1>
        <Switch>
          <Route path="/" exact component={Login}></Route>
          <Route path="/translation" component={TranslationPage}></Route>
          <Route path="/profile" component={ProfilePage}></Route>
          <Route path="*" component={NotFound}></Route>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
